<?php
function d($var){
    echo "<pre>";
    print_r($var);
    //echo __LINE__;
    echo "</pre>";
}
//One dimmensional numeric array
$fruits = array("grapes","apple","cherry","mango","watermelon","banana");
d($fruits);


foreach ($fruits as $key => $value){
    echo $key." ".$value;
    echo "<hr />";
}
?>

<body>
    <ul>
        <?php
        foreach($fruits as $key => $value){
            ?>
        <li>grapes</li>

        <?php
        }
        ?>
    </ul>
</body>
<?php
//foreach ():
//
//endforeach;


echo $fruits[0];
echo $fruits[1];
echo $fruits[2];


$ages =[
    "peter"=>35,
    "jon"=>40,
    "james"=>45
];
d($ages);
//echo $fruits[2][3];

//Two Dimmentional associative array
$films = array(
    "genres" => array("comedy","tragedy","action","romance","horror"),
    "film_titles" => array("Big","Star Wars","Titanic","Frence kiss","Sucide"),
    "stars" => array("Bill Murray","Mark Hammell","Leonard Dicaprio","Cate Blanchet","rashna"),

);

d($films);



?>
